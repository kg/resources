#!/bin/bash

INSTALL_DIR=`pwd`

install_titan() {
    titan="titan-1.0.0-hadoop1"
    cd /opt
    sudo wget http://s3.thinkaurelius.com/downloads/titan/$titan.zip
    sudo unzip $titan.zip
    cd $titan
    # echo -e "***Enter in gremlin shell:\n\tgraph = TitanFactory.build().set('storage.backend', 'inmemory').open()"
    # bin/gremlin.sh
    echo -e 'function titan() {\n\t/opt/${titan}/bin/gremlin.sh -e $1\n}' >> ~/.bashrc
    source ~/.bashrc

    sudo cp $INSTALL_DIR/conf/gremlin-server.yaml /opt/titan-1.0.0-hadoop1/conf/gremlin-server/
    sudo cp $INSTALL_DIR/conf/graph.groovy /opt/titan-1.0.0-hadoop1/scripts/
    sudo cp $INSTALL_DIR/conf/inmemory.properties /opt/titan-1.0.0-hadoop1/conf/gremlin-server/
}

install_java() {
    sudo apt-get install software-properties-common
    sudo apt-add-repository ppa:webupd8team/java
    sudo apt-get update
    sudo apt-get install oracle-java8-installer
}

init() {
    sudo apt-get update
    sudo apt-get -y upgrade
    sudo apt-get install -y vim unzip curl tmux
}

# Make sure sudo credentials are in the cache.
sudo echo "Cached"
if [ $? -ne 0 ]; then
    echo "Caching failed"
    exit 1
fi

init
install_java
install_titan
