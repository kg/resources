# Bulbs + Neo4j

Bulbs is a Python framework for graph dbs. See http://bulbflow.com/quickstart/.

Neo4j is a graph db. See http://neo4j.com/.

Run `setup.sh` to install both (tested on emulab). Make sure you run the script
from the same directory as `neo4j-server.properties` or if not, simply set the
following flag in `<neo4j_dir>/conf/neo4j-server.properties`:

```
dbms.security.auth_enabled=false
```

Neo4j server should now be running. Run sample graph:

```
python graph.py
```

Use the bulbs API to write your own scripts: http://bulbflow.com/api/bulbs/.
