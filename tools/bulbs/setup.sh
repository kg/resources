#!/bin/bash

INSTALL_DIR=`pwd`
NEO4J_VERSION=1.9.5

install_pip() {
    cd /opt
    sudo wget https://bootstrap.pypa.io/get-pip.py
    sudo python get-pip.py
}

install_bulbs() {
    sudo pip install https://github.com/espeed/bulbs/tarball/master
}

install_neo4j() {
    cd /opt
    sudo wget http://neo4j.com/artifact.php?name=neo4j-community-$NEO4J_VERSION-unix.tar.gz
    sudo mv artifact.php\?name\=neo4j-community-$NEO4J_VERSION-unix.tar.gz neo4j-community-$NEO4J_VERSION-unix.tar.gz
    sudo tar -xvzf neo4j-community-$NEO4J_VERSION-unix.tar.gz
    sudo cp $INSTALL_DIR/neo4j-server.properties /opt/neo4j-community-$NEO4J_VERSION/conf/neo4j-server.properties
    cd neo4j-community-$NEO4J_VERSION
    sudo bin/neo4j start
}

install_gremlin() {
    cd /opt
    sudo git clone https://github.com/tinkerpop/gremlin.git
    cd gremlin
    sudo mvn clean install
}

install_maven() {
    sudo apt-get install -y maven
}

install_java() {
    sudo apt-get install -y default-jdk
}

init() {
    sudo apt-get update
    sudo apt-get -y upgrade
    sudo apt-get install -y vim unzip curl tmux
    install_pip
}

# Make sure sudo credentials are in the cache.
sudo echo "Cached"
if [ $? -ne 0 ]; then
    echo "Caching failed"
    exit 1
fi

init
install_java
install_bulbs
install_maven
install_gremlin
install_neo4j
