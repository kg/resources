from bulbs.neo4jserver import Graph
g = Graph()
jack = g.vertices.create(name="Jack")
jill = g.vertices.create(name="Jill")
g.edges.create(jack, "knows", jill)
for v in g.vertices.index.lookup(name="Jack"):
    print v.name
