# Knowledge Graph Resources

A collection of useful knowledge graph papers, tutorials, etc.

## Links

Links to knowledge graph resources.

* [Naga](https://www.mpi-inf.mpg.de/departments/databases-and-information-systems/research/yago-naga/)

### Tools

Knowledge graph and graph database software.

* [TinkerPop3](http://tinkerpop.incubator.apache.org/docs/3.0.1-incubating/) -
  provides graph computing capabilities for both graph databases (OLTP) and
  graph analytic systems (OLAP) under the Apache2 license.
* [Bulbs](http://bulbflow.com/quickstart/) - a Python framework for graph dbs
  like Neo4j. Included in this repo [here](tools/bulbs/).
